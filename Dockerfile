FROM fedora:31

RUN dnf install -y gcc-9.3.1 \
        make \
        autoconf \
        automake \
        kernel-devel \
        kernel-modules-internal \
        libtool \
        readline-devel \
        diffutils \
        file \
        dbus \
        dbus-tools \
        dbus-daemon \
        dbus-libs \
        dbus-common \
        dbus-broker \
        dbus-glib \
        libnl3-devel \
        libnfnetlink-devel \
        openssl-devel \
        procps-ng \
        iproute \
        wireless-tools \
        net-tools \
        kmod \
        aircrack-ng \
        rfkill \
        bzip2 \
        wget

COPY ./build /build/ 
COPY ./data/ config.sh /PoC_iwd/
COPY ./run.sh /etc/init/run.sh

### Install third party softwares ###

# Install sexpect to 
WORKDIR /build/third_party_library/sexpect
RUN make -j && \
        cp sexpect /usr/bin

# Install hostapd with WPA3 support
WORKDIR /build/third_party_library/hostapd/hostapd
RUN [ "$(grep -E "^CONFIG_SAE=y$" .config)" ] || \
        echo "CONFIG_SAE=y" >> .config && \
        make -j && \
        make install

# Install ELL, the crypto library used by iwd
WORKDIR /build/third_party_library/ell
RUN ./bootstrap && \
        ./configure && \
        make -j && \
        make install

# Install iwd
WORKDIR /build/third_party_library/iwd
RUN ./bootstrap && \
        ./configure --disable-systemd-service --with-dbus-datadir=/usr/share/ --disable-manual-pages && \
        make -j && \
        make install

### Install PoC related binaries/scripts ###

# Compile and install the spy process
WORKDIR /build/PoC/
RUN chmod +x trace_parser.py simulation.sh && \
        make && \
        make install && \
        make clean

WORKDIR /PoC_iwd
ENTRYPOINT ["/etc/init/run.sh"]

#CMD ["dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address &"]
#CMD ["/bin/bash"]