# Content

- [Content](#content)
- [Repository layout](#repository-layout)
- [Attack on iwd](#attack-on-iwd)
	- [Core idea](#core-idea)
	- [Threat model](#threat-model)
	- [Different elements](#different-elements)
- [Testing the PoC](#testing-the-poc)
	- [Requirements](#requirements)
	- [Deploying the test environment](#deploying-the-test-environment)
	- [Running the cache attack](#running-the-cache-attack)
	- [Finding password/MAC leading to DoS](#finding-passwordmac-leading-to-dos)
- [Patch](#patch)

----------------

**Disclamer:** It is worth noting that the nature of our attack (cache side-channels) makes the exact outcome of the spy process dependent of the executing CPU, which might lead to less reliable results. We included a sample of our results in [data/results](data/results).

----------------


# Repository layout

* [build](build/) contains sources to build third party libraries (namely the targeted version of iwd) as long as all elements needed to execute the PoC. This directory is only used when building the container to setup the test environment.
* [data](data/) contains some additional data we used (such as some password dictionaries) and generated (traces we collected using our spy process, or password causing a Denial of Service when set with particular MAC addresses).

# Attack on iwd 

## Core idea

This cache attack targets the Dragonfly exchange implemented in iwd. It exploits password / control flow dependency in the implementation. This dependency occurs in the hunting and pecking procedure used to convert the password into an elliptic curve point. 
Due to the try-and-increment nature of this procedure, attackers can guess at which iteration the password has successfully been converted into a point, thus reducing the number of possible passwords.

Since the conversion depends not only on the password, but also on both parties' MAC addresses, acquiring several measures on the same password and different addresses allow attackers to tremendously reduce the size of the dictionary.

## Threat model

To exploit it, the attackers need to be able to monitor the CPU cache, using a classical Flus+Reload attack for instance. To do so, we assume that the attackers are able to deploy a spy process, with no specific privileges other than being able to read the iwd binary (which is a default permissions).

This spy process is assumed to run in background in order to record the CPU cache access to some specific functions.

## Different elements

In order to perform the attack, we implemented various elements, gathered in [build/PoC/](build/PoC/):

* `spy_process` allows to monitor access to some specified memory lines, and to perform a Performance Degradation Attack (using Mastik implementation). The spy collects some information (which memory line is accessed, and the time between two access) into a file (see [data/results/res_traces/](data/results/res_traces/)). This binary is expected to run in background when iwd is connecting to the Access Point. See Section 3.3 and 4.2.
* `trace_parser.py` is a script taking the output of the spy, parsing it, and returning the most probable iteration at which the password was converted. If the result is uncertain, the trace is ignored. We used it to produce the results in Section 4.3 and in Listing 3.
* `dict_reducer` takes a password dictionary and the output of trace_parser, and eliminates all passwords from the dictionary that do not fit the traces. It can be used offline by the attackers, after obtaining the outcome of the spy process. See Section 3.5.
* `simulation.sh` is the core of the PoC, gathering the previous elements to simulate the entire attack using a simple command line. It creates virtual interfaces and run hostap (as an AP) and iwd (as a client). It then toggles different connection by the client with a given password, with the spy_process running in background. The resulting spied data are processed by trace_parser to get the most probable iteration for each MAC addresses couples. Using the parsed traces, it calls the dictionary reducer to prune all passwords which yield a different iteration number for the given MAC addresses.
* `find_buggy_password` is the tool used to find a list of passwords that cannot be used with certain MAC addresses (Section 3.4 and Appendix A), thereby leading to some Denial of Service attacks.  

The following representation gives a rough idea of the attack process, and the role of each element of our Proof of Concept. All steps bellow are performed transparently by our simulation script.
![attack_idea](./.attack.jpg)


# Testing the PoC

## Requirements

We build a Dockerfile in order to avoid all dependencies issues. Hence, you need to [install docker on your platform](https://docs.docker.com/engine/install/) and make sure to start the daemon.

Since  docker relies on the host system, and we need to interact with the host network, we advise you to test our PoC on a Linux-based system, to avoid any issue. We successfully tested it on Fedora 31 and Debian 10.

Since we need to emulate some Wi-Fi interfaces, a Wi-Fi card is needed.

## Deploying the test environment

To set up the  test environment, you need to build the container (only once) by running the following command in a shell, in this directory:
```bash
$ sudo docker build --rm -t poc_iwd .
```
Building can take some time (up to two minutes on our test machine) since all dependencies need to be built and installed. Then, to enter the test environment, run the following command in your shell:
```bash
$ sudo docker run --privileged --net=host -it poc_iwd
```
You should get a shell inside the container, in the right directory (`/PoC_iwd/`). The following section assumes that you are inside the container. You can leave it anytime using the `Ctrl+D` shortcut.

## Running the cache attack

The simulation can take various arguments, allowing to perform different steps of the attack:
```
Usage: ./build/PoC/simulation.sh [-d] [-n N_TRACES] -a PASSWD DICT | -s PASSWD | -p TRACES_DIR | -r DICT TRACES
	-a: simulate the attack on PASSWD, parse the resulting trace to guess the result and run the dictionary reduction using DICT as a reference and the gathered information to prune invalid passwords. Same as -sp followed by -r.
	-s: simulate the attack on PASSWD (i.e.: collect traces).
	-p: parse the traces located in TRACES_DIR. This directory should contains a subdirectory for each address couple. Each subdirectory must contain two file: 
		* trace: contains the trace
		* debug: contains additional information such as MAC addresses and real number of rounds in debug mode (see -d).
	-r: run the dictionary reduction, testing the correspondence between all password in a dictionary to the given traces. It may take some time to execute if the dictionary contains a lot of passwords.
	-d: run in debug mode. Display additional information (namely during the parsing). This option shall not be used with -a, since the input for the dictionary reduction would not have the proper format.
	-n: Combined with -s (resp. -p) it defines the number of traces to acquire (resp. parsed) for each MAC couple.
```

A full attack (from spying the key exchange to the offline dictionary attack) can be simulated for a given password and dictionary using the following command:
```bash
$ simulation.sh -a <test_password> <dictionary_path>
```
A full attack may take some time, since acquiring trace takes a few minutes, and so do the dictionary reduction (the duration may vary along with the dictionary size and the number of available threads).

The same result can be achieved one step at a time using the following procedure.

First, run the simulation while spying the cache interaction (done by spy_process in background) in order to acquire some trace. To emulate some connections using the password "`test_password`", while spying the appropriate memory lines, run the following command:
```bash
$ simulation.sh -s test_password
```

This will create virtual interfaces, start the AP, the client and the spy. Then it will toggle a connection from the client to the AP using the given password. This connection is repeated `x` times, where `x` can be set using `-n x` (15  by default). This operation is repeated 10 times, while varying the MAC address in order to get more information (if a MAC address changes, the number of iteration needed to convert the password may also change).

-----
**Troubles with mac80211_hwsim:** We have been notified that using modprobe mac80211_hwsim inside the docker sometimes leads to an error, making the rest of the simulation imposssible. We are currently working on a fix, but as a workaround, you may create the virtual interfaces directly from your host using the following command:
```bash
$ sudo modprobe mac80211_hwsim radios=3
```
Virtual interfaces `wlan1` and `wlan2` should then be displayed in the output of `iwconfig`. Sorry for the inconvenience if you ran into such issue.

-----

At first, the daemons may need some time to start, so the script may be stuck at "Waiting for iwd to start..." for a few seconds, but should carry on. You can ignore the output in the shell since it is only used for debug purposes, and is irrelevant to the attack. Only the files created by our `spy_process` matter.

Then, you should get a directory `res_traces` containing a subdirectory named after the password you just used (`test_password` in our example). For each tested MAC address, a new directory is created with a debug file (used to check our result, and containing the MAC addresses) and the trace generated by the spy.

**If traces are too noisy, or empty (sometimes a daemon crashes for some reasons), the trace is removed automatically. In this case, we advise to run the data acquisition part again, until you have enough traces (if there is no traces left, the rest of the attack cannot apply). You need at least on folder in res_traces/\<password\> to continue.**

This traces can be parsed and interpreted by running the following command:
```bash
$ simulation.sh -p res_traces/test_password
```
This command should output the guessed number of iteration needed to convert the password for a specific MAC address. It will be used as the main input to perform the dictionary reduction.
It should look like
```bash
$ simulation.sh -p results/res_traces/example_1/
8A6444B8CDAD,62DC42366D8D,5 52F350DA9E8B,5E4B5A3FC08E,4 EAE030456914,BAF6598DE8C5,3 46C18ABCD40E,BAF6598DE8C5,3 0A8BF1135613,62DC42366D8D,2 DAF2A76BDC06,9AC0AF9C1D32,1 CEA7F2C7F2D4,62DC42366D8D,1 9AAEA5DFC1B5,BAF6598DE8C5,1 7AA3AF2C1FE1,62DC42366D8D,1 4A00DACAA6C0,BAF6598DE8C5,1
```

To get more details about the trace interpretation, you may run `simulation.sh` with `-d` which will print additional information such as the most probable iterations (instead of only one), and inconclusive results:
```bash
$ simulation.sh -d -p results/res_traces/example_1
results/res_traces/example_1/1: 
	* 52F350DA9E8B
	* 5E4B5A3FC08E
	* 4 (54.39%) - 2 (21.38%) - 1 (14.55%)
results/res_traces/example_1/10: 
	* CEA7F2C7F2D4
	* 62DC42366D8D
	* 1 (83.41%) - 2 (11.29%)
results/res_traces/example_1/2: 
	* 4A00DACAA6C0
	* BAF6598DE8C5
	* 1 (62.52%) - 4 (18.58%)
results/res_traces/example_1/3: 
	* 9AAEA5DFC1B5
	* BAF6598DE8C5
	* 1 (64.38%) - 2 (19.14%)
results/res_traces/example_1/4: 
	* EAE030456914
	* BAF6598DE8C5
	* 3 (62.06%) - 6 (21.21%) - 12 (5.47%)
results/res_traces/example_1/5: 
	* 46C18ABCD40E
	* BAF6598DE8C5
	* 3 (79.37%)
results/res_traces/example_1/6: 
	* DAF2A76BDC06
	* 9AC0AF9C1D32
	* 1 (81.36%)
results/res_traces/example_1/7: 
	* 8A6444B8CDAD
	* 62DC42366D8D
	* 5 (59.45%) - 4 (15.91%) - 7 (13.72%)
results/res_traces/example_1/8: 
	* 7AA3AF2C1FE1
	* 62DC42366D8D
	* 1 (43.48%) - 5 (21.56%) - 13 (7.35%) - 11 (6.52%)
results/res_traces/example_1/9: 
	* 0A8BF1135613
	* 62DC42366D8D
	* 2 (63.96%) - 1 (22.34%) - 3 (10.42%)
8A6444B8CDAD,62DC42366D8D,5 52F350DA9E8B,5E4B5A3FC08E,4 EAE030456914,BAF6598DE8C5,3 46C18ABCD40E,BAF6598DE8C5,3 0A8BF1135613,62DC42366D8D,2 DAF2A76BDC06,9AC0AF9C1D32,1 CEA7F2C7F2D4,62DC42366D8D,1 9AAEA5DFC1B5,BAF6598DE8C5,1 7AA3AF2C1FE1,62DC42366D8D,1 4A00DACAA6C0,BAF6598DE8C5,1
```

At last, use the previous output to reduce the possible passwords, running the following command:
```bash
$ simulation.sh -r <dictionary> <MAC_A,MAC_B,n> [<MAC_A,MAC_B,n> ...] 
```
to output passwords in the dictionary satisfying the same traces. In spite of CPU parallelization, processing large password dictionary may take some time (the following example takes approximately 7 minutes on an 8-threads CPU), so you may reduce the dictionary size to test it.

For instance, 
```
$ simulation.sh -r resources/rockyou_8_chars_and_more.txt 8A6444B8CDAD,62DC42366D8D,5 52F350DA9E8B,5E4B5A3FC08E,4 EAE030456914,BAF6598DE8C5,3 46C18ABCD40E,BAF6598DE8C5,3 0A8BF1135613,62DC42366D8D,2 DAF2A76BDC06,9AC0AF9C1D32,1 CEA7F2C7F2D4,62DC42366D8D,1 9AAEA5DFC1B5,BAF6598DE8C5,1 7AA3AF2C1FE1,62DC42366D8D,1 4A00DACAA6C0,BAF6598DE8C5,1
8A6444B8CDAD,62DC42366D8D,5 52F350DA9E8B,5E4B5A3FC08E,4 EAE030456914,BAF6598DE8C5,3 46C18ABCD40E,BAF6598DE8C5,3 0A8BF1135613,62DC42366D8D,2 DAF2A76BDC06,9AC0AF9C1D32,1 CEA7F2C7F2D4,62DC42366D8D,1 9AAEA5DFC1B5,BAF6598DE8C5,1 7AA3AF2C1FE1,62DC42366D8D,1 4A00DACAA6C0,BAF6598DE8C5,1
```

yields 6 different possible passwords (from the 9'605'058 initial possibilities), which can be tested manually to determine which one is the correct one.

## Finding password/MAC leading to DoS

A large dictionary is available in [data/resources](data/resources). Run the following command from the shell:
```bash
$ find_buggy_passwords resources/huge_dictionary.txt > passwords_causing_dos.txt
```
This command can load a significant amount of data in RAM, so you may need to replace `huge_dictionary.txt` by `rockyou_8_chars_and_more.txt` if it crashes. It is also designed to run indefinitely, generating a new MAC addresses tuple when it has tested all the dictionary.

Depending on how many CPU cores are available, "buggy" passwords are expected to be found in 30 seconds to 2 minutes on average.

# Patch

A well known and good mitigation to this vulnerability is to switch to a deterministic password to point conversion function. However, in order to keep the protocol backward-compatible, we proposed a branch-free implementation of the conversion function to integrate in iwd. After some discussions, the patch was applied on both iwd ([commit 211f7dde6e87b4ab52430c983ed75b377f2e49f1](https://git.kernel.org/pub/scm/network/wireless/iwd.git/commit/?id=211f7dde6e87b4ab52430c983ed75b377f2e49f1)) and ell ([commit 47c2afeec967b83ac53b5d13e8f2dc737572567b](https://git.kernel.org/pub/scm/libs/ell/ell.git/commit/?id=47c2afeec967b83ac53b5d13e8f2dc737572567b)).

This PoC only targets iwd implementation, but other implementations are vulnerable. We also designed a PoC targeting FreeRadius' EAP-pwd implementation, and integrated a similar patch ([commit 6f0e0aca4f4e614eea4ce10e226aed73ed4ab68b](https://github.com/FreeRADIUS/freeradius-server/commit/6f0e0aca4f4e614eea4ce10e226aed73ed4ab68b)). The PoC being quite similar, we did not include it in this repository.
