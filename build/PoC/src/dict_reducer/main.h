#ifndef DICT_REDUCER_MAIN_H
#define DICT_REDUCER_MAIN_H

#include <ell/ell.h>
#include <stdint.h>

#include "crypto.h"

struct sae_sm {
    struct handshake_state *handshake;
    struct l_ecc_point *pwe;
    const struct l_ecc_curve *curve;
    unsigned int group;
    uint8_t group_retry;
    const unsigned int *ecc_groups;
    struct l_ecc_scalar *rand;
    struct l_ecc_scalar *scalar;
    struct l_ecc_scalar *p_scalar;
    struct l_ecc_point *element;
    struct l_ecc_point *p_element;
    uint16_t send_confirm;
    uint8_t kck[32];
    uint8_t pmk[32];
    uint8_t pmkid[16];
    uint8_t *token;
    size_t token_len;
    /* number of state resyncs that have occurred */
    uint16_t sync;
    /* number of SAE confirm messages that have been sent */
    uint16_t sc;
    /* received value of the send-confirm counter */
    uint16_t rc;
    /* remote peer */
    uint8_t peer[6];
    void *user_data;
};

typedef struct {
    uint8_t A[6];
    uint8_t B[6];
    uint8_t x;
} Trace;

#endif
