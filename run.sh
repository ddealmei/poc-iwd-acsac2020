#! /bin/bash
rfkill unblock wifi
[ "$(ip a | grep  wlan1)" ] && iwconfig wlan1 channel 1
[ "$(ip a | grep  wlan2)" ] && iwconfig wlan2 channel 1
dbus-uuidgen > /var/lib/dbus/machine-id
mkdir /var/run/dbus
dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address &
/bin/bash
